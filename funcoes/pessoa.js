var Pessoa = /** @class */ (function () {
    function Pessoa(nome) {
        this.nome = nome;
    }
    return Pessoa;
}());
var pessoa = new Pessoa('Alexandre');
console.log('Instancia de pessoa:', pessoa);
