class Pessoa {
    constructor(nome) {
        this.nome = nome
    }

    falar() {
        console.log(`Meu nome eh ${this.nome}`)
    }
}

const p1 = new Pessoa('Joao')
p1.falar()

const criarPessoa = nome => {
    return {
        falar: () => console.log(`Meu nome eh ${nome}`)
        // nao precisa usar o 'this' pq a variavel tem consciencia
        // do contexto lexico em que ela esta inserida.
        // Nao corre o risco de dar um 'undefined'
        // ao rodar a aplicacao no browser.
    }
}

const p2 = criarPessoa('Joana')
p2.falar()