let a = 3

global.b = 234213

this.c = 456457
this.d = false
this.e = 'teste'

console.log(a)
console.log(global.b)
console.log(this.c)
console.log(module.exports.c)
console.log(module.exports === this)
console.log(module.exports)

// criando uma variavel maluca: sem var e sem let!
abc = 3 // nao fazer isso em casa !!

console.log(global.abc)

// module.exports = { e: 456, f: false, g: 'teste'}