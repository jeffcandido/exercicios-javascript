// Eh possivel alterar valores de atributos de objetos constantes
// Nao eh possivel alterar os enderecos de memoria para o qual
// eles apontam.

// pessoa -> 123 -> {...}

const pessoa = { nome: 'Joao'}
pessoa.nome = 'Pedro'
console.log(pessoa)

// pessoa -> 456 -> {...} --- uma nova atribuicao gera erro
// pessoa = { nome: 'Ana'}

Object.freeze(pessoa) // faz ignorar alteracoes posteriores

pessoa.nome = 'Maria'
pessoa.end = 'Rua ABC'

delete pessoa.nome

console.log(pessoa.nome)
console.log(pessoa)

const pessoaConstante = Object.freeze({ nome: 'Joao'})
pessoaConstante.nome = 'Maria'  // nao eh possivel alterar esse atributo
console.log(pessoaConstante)