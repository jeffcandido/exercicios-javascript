// CODIGO NAO EXECUTAVEL!!!

// Procedural
processamento(valor1, valor2, valor3)

// Orientacao a Objetos

objeto = {
    valor1,
    valor2,
    valor3,

    processamento() {
        // ...
    }

}

objeto.processamento() // O foco passou a ser o objeto

// Principios importantes

// 1. Abstracao
// 2. Encapsulamento
// 3. Heranca (prototype)
// 4. Polimorfismo