console.log(typeof String)
console.log(typeof Array)
console.log(typeof Object)

String.prototype.reverse = function() {
    return this.split('').reverse().join('')
}

console.log('Estudos do Jeff!!!'.reverse())

Array.prototype.first = function() {
    return this[0]
}
console.log([1, 2, 3, 4, 5].first())
console.log(['a', 'b', 'c', 'd'].first())

// nao eh recomendado, mas da pra substituir funcoes que ja existem 
String.prototype.toString = function () {
    return 'Lascou Tudo =('
}

console.log('Estudos do Jeff!!!'.reverse())