const nums = [1, 2, 3, 4, 5]

// For com proposito
let resultado = nums.map(function(e) {
    return e * 2
})

console.log(resultado)

// O `map` nao modifica o array atual
// ele cria um array novo

const som10 = e => e + 10
const triplo = e => e * 3
const paraDinheiro = e => `R$ ${parseFloat(e).toFixed(2).replace('.', ',')}`

result = nums.map(som10).map(triplo).map(paraDinheiro)

console.log(result)
