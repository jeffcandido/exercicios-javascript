const produtos = [
    { nome: 'Notebook', preco: 2499, fragil: true },
    { nome: 'iPad Pro', preco: 4199, fragil: true },
    { nome: 'Copo de Vidro', preco: 12.49, fragil: true },
    { nome: 'Copo de Plastico', preco: 18.99, fragil: true }
]

// recebe elemento, indice e o array original
// retorna um array 'filtrado'

console.log(produtos.filter(function(p) {
    return true
}))

console.log(produtos.filter(function(p) {
    return false
}))

console.log(produtos.filter(function(p) {
    return p.preco > 2500
}))

// fazendo direto
console.log(produtos.filter(function(p) {
    return (p.preco > 500 && p.fragil === true)
}))

// fazendo funcoes separadas

const caro = produto => produto.preco >= 500
const fragile = produto => produto.fragil

console.log(produtos.filter(caro).filter(fragile))