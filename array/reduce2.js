const alunos = [
    { nome: 'Joao', nota: 7.3, bolsista: false},
    { nome: 'Maria', nota: 9.2, bolsista: true},
    { nome: 'Pedro', nota: 9.8, bolsista: false},
    { nome: 'Ana', nota: 8.7, bolsista: true}
]

console.log(alunos.map(a => a.bolsista))


// MEU CODIGO
//Desafio1: 
console.log(`Todos os alunos sao bolsistas?`)
const resultado = alunos.map(a => a.bolsista).reduce(function(acumulador, atual) {
    // console.log(acumulador, atual)
    return acumulador && atual
}, true)
console.log(resultado)

//Desafio1: 
console.log(`Algum aluno eh bolsista?`)
const resultado2 = alunos.map(a => a.bolsista).reduce(function(acumulador, atual) {
    // console.log(acumulador, atual)
    return acumulador || atual
}, true)
console.log(resultado2)

// CODIGO DO PROFESSOR
// Desafio 1: Todos os alunos sao bolsistas?
const todosBolsistas = (result, bolsist) => result && bolsist
console.log(alunos.map(a => a.bolsista).reduce(todosBolsistas))

//Desafio 2: Algum aluno eh bolsista?
const algumBolsista = (result, bolsist) => result || bolsist
console.log(alunos.map(a => a.bolsista).reduce(algumBolsista))